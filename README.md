# Uber POC

A mobile app that uses the Flickr image search API and shows the results in a 3-column scrollable view


This project is built using MVVM design pattern.

Project configurations are as follow,

Xcode Version - 10.2.1
Swift Version - 5
deployment Target - iOS 10
