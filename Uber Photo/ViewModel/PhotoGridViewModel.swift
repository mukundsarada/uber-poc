//
//  PhotoGridViewModel.swift
//  Uber Photo
//
//  Created by Mukund Sarada on 19/05/19.
//  Copyright © 2019 Uber. All rights reserved.
//

import Foundation

protocol PhotoGridViewModelDelegate: class {
    
    ///
    /// This method is called when new set of images are ready
    /// to be loaded on UI
    ///
    func didUpdateImageItemList()
    
    ///
    /// This method is called when API failed to fetch image list
    ///
    /// - parameter error: Error while fetching image list
    /// - parameter forPage: Page index for which request failed
    ///
    func didFailTofetchImageItemList(withError error: Error?, forPage page: Int)
}

class PhotoGridViewModel {
    
    weak var delegate: PhotoGridViewModelDelegate?
    var networkManager: NetworkManager
    
    /// Indicates index of latest page fetched
    var curentPageIndex: Int
    
    /// Search text
    var searchText: String
    
    /// Fetched image list
    var imageItems: [GridImage]
    
    var responseModel: FlickrSearchResponse?
    
    /// Will be set to true when request is in
    /// progress. Otherwise, set to false
    var isNextPageRequestInProgress: Bool
    
    required init(delegate: PhotoGridViewModelDelegate) {
        self.delegate = delegate
        curentPageIndex = 1
        searchText = ""
        networkManager = NetworkManager()
        imageItems = []
        isNextPageRequestInProgress = false
    }
    
    ///
    /// Searches images for input text.
    ///
    /// - parameter searchText: Text for which images to be searched.
    ///
    func getImagesForSearchText(_ searchText: String?) {
        if let searchText = searchText, searchText.count > 0 {
            if self.searchText != searchText  {
                clearSearchResult()
                self.searchText = searchText
                fetchResultFor(serachText: searchText, page: curentPageIndex)
            }
        } else {
            clearSearchResult()
        }
    }
    
    ///
    /// Clears fetched image list.
    ///
    func clearSearchResult() {
        curentPageIndex = 1
        isNextPageRequestInProgress = false
        self.searchText = ""
        imageItems.removeAll()
        ImageCacheManager.shared.clearCache()
    }
    
    ///
    /// Fetches images from next page. To be called when
    /// scrolled to the end of page
    ///
    func searchImagesOnNextPage() -> Bool {
        var isRequestStarted = false
        if (isNextPageRequestInProgress == false) && (self.searchText != "") {
            if let responseModel = responseModel,
                let totalPages = Int(responseModel.total),
                totalPages > curentPageIndex {
                curentPageIndex += 1
                fetchResultFor(serachText: searchText, page: curentPageIndex)
                isRequestStarted = true
            }
        }
        return isRequestStarted
    }

    ///
    /// Fetches new set of images from Server.
    /// On success, update the UI with updated image list.
    /// On error, show the alert if page index is 1.
    ///
    private func fetchResultFor(serachText: String, page: Int) {
        isNextPageRequestInProgress = true
        let request = FlickrImageSoure.urlRequestFor(searchText: searchText, page: curentPageIndex)!
        networkManager.fetch(with: request, type: FlickrSearchResponse.self) { [weak self] (response, error) in
            guard let weakSelf = self else {
                return
            }
            weakSelf.isNextPageRequestInProgress = false
            guard let response = response, !response.images.isEmpty else {
                if weakSelf.curentPageIndex > 1 {
                    weakSelf.curentPageIndex -= 1
                }
                DispatchQueue.main.async {
                    weakSelf.delegate?.didFailTofetchImageItemList(withError: error, forPage: page)
                }
                return
            }
            weakSelf.imageItems.append(contentsOf: response.images)
            weakSelf.responseModel = response
            DispatchQueue.main.async {
                weakSelf.delegate?.didUpdateImageItemList()
            }
        }
    }
}
