//
//  ViewController.swift
//  Uber Photo
//
//  Created by Mukund Sarada on 19/05/19.
//  Copyright © 2019 Uber. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchBar: UISearchBar!

    let imageHeightToWidthRatio: CGFloat = 1.33
    let imageDetailPlaceholderHeight: CGFloat = 24
    private let numberOfColoumns: Int = 3
    let minSpacing: Int = 10
    var viewModel: PhotoGridViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCustomCell()
        viewModel = PhotoGridViewModel.init(delegate: self)
    }
    
    private func registerCustomCell() {
        let cellNib = UINib(nibName: PhotoGridViewCell.identifier, bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: PhotoGridViewCell.identifier)
    }
}

//MARK:- UICollectionViewDataSource Methods
extension ViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let viewModel = viewModel  else {
            return 0
        }
        return viewModel.imageItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard viewModel.imageItems.count > indexPath.row else {
            return UICollectionViewCell()
        }
        let imageItem = viewModel.imageItems[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:PhotoGridViewCell.identifier,
                                                      for: indexPath) as! PhotoGridViewCell
        cell.updateWithData(imageItem)
        return cell
    }
}

//MARK:- UICollectionViewDelegateFlowLayout Methods
extension ViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if numberOfColoumns == 0 {
            return  CGSize.zero
        }
        let totalWidth = view.frame.width
        let totalPadding = (numberOfColoumns + 1) * minSpacing
        let widthWithoutPadding = Int(totalWidth) - totalPadding
        let cellWidth = widthWithoutPadding/numberOfColoumns
        let imageHeight = CGFloat(cellWidth) * imageHeightToWidthRatio
        let cellHeigth = imageHeight + imageDetailPlaceholderHeight
        return  CGSize(width: CGFloat(cellWidth), height: cellHeigth)
    }
}

//MARK:- UIScrollViewDelegate
extension ViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            let isRequestStarted = viewModel?.searchImagesOnNextPage()
            if isRequestStarted == true {
                activityIndicator.startAnimating()
            }
        }
    }
}

//MARK:- PhotoGridViewModelDelegate
extension ViewController: PhotoGridViewModelDelegate {
    func didUpdateImageItemList() {
        collectionView.reloadData()
        activityIndicator.stopAnimating()
        guard let viewModel = viewModel else {
            return
        }
        if viewModel.imageItems.count == 0 {
            showAlert(withTitle: nil, message: NSLocalizedString("NO_RESULTS", comment: ""))
        }
    }
    
    func didFailTofetchImageItemList(withError error: Error?, forPage page: Int) {
        activityIndicator.stopAnimating()
        if page == 1 {
            let title: String = NSLocalizedString("ERROR_TITLE", comment: "")
            var message: String = NSLocalizedString("ERROR_MESSAGE", comment: "")
            if let error = error {
                message = error.localizedDescription
            }
            showAlert(withTitle: title, message: message)
        }
    }
    
    private func showAlert(withTitle title: String?, message: String) {
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAlertAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""),
                                              style: .cancel,
                                              handler: nil)
        alertController.addAction(cancelAlertAction)
        present(alertController, animated: true, completion: nil)
    }
}

//MARK:- UISearchBarDelegate Search Bar intercation Methods
extension ViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if let text = searchBar.text, text != "", let viewModel = viewModel, viewModel.searchText != text {
            activityIndicator.startAnimating()
            collectionView.reloadData()
            viewModel.getImagesForSearchText(text)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let viewModel = viewModel else { return }
        if viewModel.imageItems.count > 0 {
            viewModel.clearSearchResult()
            collectionView.reloadData()
        }
    }
}
