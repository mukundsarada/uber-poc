//
//  FlickrImageSearchResponse.swift
//  Uber Photo
//
//  Created by Mukund Sarada on 19/05/19.
//  Copyright © 2019 Uber. All rights reserved.
//

import Foundation

struct FlickrSearchResponse: Decodable {
    let page: Int
    let pages: Int
    let perpage: Int
    let total: String
    let images: [FlickrImage]
    
    enum CodingKeys: String, CodingKey {
        case photos
    }
    
    enum PageResponseKeys: String, CodingKey {
        case page
        case pages
        case perpage
        case total
        case images = "photo"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let pageDetails = try values.nestedContainer(keyedBy: PageResponseKeys.self, forKey: .photos)
        page = try pageDetails.decode(Int.self, forKey: .page)
        pages = try pageDetails.decode(Int.self, forKey: .pages)
        perpage = try pageDetails.decode(Int.self, forKey: .perpage)
        total = try pageDetails.decode(String.self, forKey: .total)
        images = try pageDetails.decode([FlickrImage].self, forKey: .images)
    }
}

struct FlickrImage: Decodable, GridImage {
    let id: String
    let farm: Int
    let server: String
    let secret: String
    let title: String?
    let owner: String?
    let isPublic: Int?
    let isFriend: Int?
    let isFamily: Int?
    var imageURL: URL? {
        let urlString = String(format: NetworkConstants.flickrImageURL, farm, server, id, secret)
        return URL(string: urlString)
    }
    
    var imageTitle: String? {
        return title
    }

    enum CodingKeys: String, CodingKey {
        case id
        case farm
        case server
        case secret
        case title
        case owner
        case isPublic
        case isFriend
        case isFamily
    }
}

protocol GridImage {
    var imageURL: URL? {get}
    var imageTitle: String? {get}
}
