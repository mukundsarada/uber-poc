//
//  UIImageView+Extension.swift
//  Uber Photo
//
//  Created by Mukund Sarada on 19/05/19.
//  Copyright © 2019 Uber. All rights reserved.
//

import UIKit

extension UIImageView {
    func loadImage(_ url: URL, placeholder: UIImage? = nil, completion: ((UIImage?, URL) -> Void)? = nil) {
        ImageCacheManager.shared.loadImage(url, placeholder: placeholder) { (image, requestURL) in
            completion?(image, requestURL)
        }
    }
}
