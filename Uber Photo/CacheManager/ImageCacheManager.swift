//
//  ImageCacheManager.swift
//  Uber Photo
//
//  Created by Mukund Sarada on 19/05/19.
//  Copyright © 2019 Uber. All rights reserved.
//

import UIKit

class ImageCacheManager {
    static let shared = ImageCacheManager()
    private let fileManager: FileManager
    private let networkManager: NetworkManager
    private let cache: NSCache<NSString, UIImage>
    
    private init() {
        fileManager = FileManager.default
        networkManager = NetworkManager()
        cache = NSCache<NSString, UIImage>()
        cache.name = "UberPOCImageCache"
        cache.countLimit = 300
    }
    
    ///
    /// Fetches image either from cache or from server.
    /// If image is available in the cache, this function returns image from cache
    ///
    /// - parameter url: image request URL.
    /// - parameter placeholder: Placeholder Image.
    /// - parameter completion: The block to be invoked on request completion.
    ///
    func loadImage(_ url: URL, placeholder: UIImage? = nil, completion: @escaping (UIImage?, URL) -> Void) {
        if let cachedImage = cache.object(forKey: url.absoluteString as NSString) {
            completion(cachedImage, url)
        } else {
            let request = URLRequest(url: url)
            networkManager.fetchImage(with: request) { [weak self] (imageData, error) in
                if let imageData = imageData {
                    if let image = UIImage(data: imageData) {
                        self?.cache.setObject(image, forKey: url.absoluteString as NSString)
                        DispatchQueue.main.async {
                            completion(image, url)
                        }
                        return
                    }
                }
                DispatchQueue.main.async {
                    completion(placeholder, url)
                }
            }
        }
    }
    
    ///
    /// Clears all the cached images
    ///
    func clearCache() {
        cache.removeAllObjects()
    }
}
