//
//  PhotoGridViewCell.swift
//  Uber Photo
//
//  Created by Mukund Sarada on 19/05/19.
//  Copyright © 2019 Uber. All rights reserved.
//

import UIKit

class PhotoGridViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var cellTitleLabel: UILabel!
    
    var requestedImageURL: URL?
    let placeholderImage: UIImage = UIImage(named: "placeholder")!
    var dataModel: GridImage?
    
    static let identifier = "PhotoGridViewCell"
    
    override func awakeFromNib() {
        cellImageView.layer.borderWidth = 0.5
        cellImageView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func updateWithData(_ dataModel: GridImage) {
        resetUI()
        if let imageURL = dataModel.imageURL {
            requestedImageURL = imageURL
            cellImageView.loadImage(imageURL, placeholder: placeholderImage, completion: { [weak self] (image, url) in
                if let currentURL = dataModel.imageURL, currentURL.absoluteString == url.absoluteString {
                    self?.cellImageView.image = image
                } else {
                    self?.cellImageView.image = self?.placeholderImage
                }
            })
        }
        if let title = dataModel.imageTitle {
            cellTitleLabel.text = title
        }
    }
    
    func resetUI() {
        requestedImageURL = nil
        cellImageView.image = placeholderImage
        cellTitleLabel.text = ""
    }
}
