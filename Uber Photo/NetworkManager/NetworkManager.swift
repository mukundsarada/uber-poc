//
//  NetworkManager.swift
//  Uber Photo
//
//  Created by Mukund Sarada on 19/05/19.
//  Copyright © 2019 Uber. All rights reserved.
//

import Foundation

protocol ServiceProtocol: class {
    func fetch<T: Decodable>(with request: URLRequest,
                  type: T.Type,
                  completion: @escaping (T?, Error?) -> Void)
    func fetchImage(with request: URLRequest,
                    completion: @escaping (Data?, Error?) -> Void)
}

class NetworkManager: ServiceProtocol {
    
    let session: URLSession
    
    init() {
       session  = URLSession(configuration: .default)
    }
    
    ///
    /// Fetches search data from server and decodes value of the given type from fetched data.
    ///
    /// - parameter request: Request to fetch data from.
    /// - parameter type: The type of the value to decode.
    /// - parameter completion: The block to be invoked on request completion.
    ///
    func fetch<T: Decodable>(with request: URLRequest,
                             type: T.Type,
                             completion: @escaping (T?, Error?) -> Void) {
        
        let task = session.dataTask(with: request) { data, response, error in
            guard let data = data else {
                completion(nil, error)
                return
            }
            do {
                let genericModel = try JSONDecoder().decode(T.self, from: data)
                completion(genericModel, nil)
            } catch {
                completion(nil, error)
            }
        }
        task.resume()
    }
    
    ///
    /// Fetches image data from server.
    ///
    /// - parameter request: Request to fetch data from.
    /// - parameter completion: The block to be invoked on request completion.
    ///
    func fetchImage(with request: URLRequest, completion: @escaping (Data?, Error?) -> Void) {
        let task = session.dataTask(with: request) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(nil, error)
                return
            }
            if httpResponse.statusCode == 200, error == nil {
                if let data = data {
                    completion(data, nil)
                } else {
                    completion(nil, error)
                }
            } else {
                completion(nil, error)
            }
        }
        task.resume()
    }
}
