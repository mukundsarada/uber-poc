//
//  Constants.swift
//  Uber Photo
//
//  Created by Mukund Sarada on 19/05/19.
//  Copyright © 2019 Uber. All rights reserved.
//

import Foundation

struct NetworkConstants {
    static let flickrImageURL: String = "https://farm%d.static.flickr.com/%@/%@_%@.jpg"
    static let flickrImageSearchURL: String = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=3e7cc266ae2b0e0d78e279ce8e361736&format=json&nojsoncallback=1&safe_search=1"
}
