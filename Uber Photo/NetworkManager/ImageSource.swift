//
//  Util.swift
//  Uber Photo
//
//  Created by Mukund Sarada on 19/05/19.
//  Copyright © 2019 Uber. All rights reserved.
//

import Foundation

/*
 * The ImageSource protocol is to be implemented by data sources.
 */
protocol ImageSource {
    
    ///
    /// Builds the url request for registered data source.
    ///
    /// - parameter searchText: Searched text.
    /// - parameter page: Page Int.
    /// - returns: url request
    ///
    static func urlRequestFor(searchText: String, page: Int) -> URLRequest?
}

struct FlickrImageSoure: ImageSource {
    static func urlRequestFor(searchText: String, page: Int) -> URLRequest? {
        var url: URL = URL(string: NetworkConstants.flickrImageSearchURL)!
        guard let components = NSURLComponents(url: url, resolvingAgainstBaseURL: false) else {
            return nil
        }
        guard var queryItems = components.queryItems else { return nil }
        queryItems = queryItems.filter {$0.name != "text"}
        let pageQueryItem = URLQueryItem(name: "page", value: String(page))
        let textQueryItem = URLQueryItem(name: "text", value: searchText)
        queryItems.append(pageQueryItem)
        queryItems.append(textQueryItem)
        components.queryItems = queryItems
        url = components.url!
        return URLRequest.init(url: url)
    }
}
