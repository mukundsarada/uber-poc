//
//  PhotoGridViewModelTests.swift
//  Uber PhotoTests
//
//  Created by Mukund Sarada on 20/05/19.
//  Copyright © 2019 Uber. All rights reserved.
//

import XCTest
@testable import Uber_Photo

class PhotoGridViewModelTests: XCTestCase {
    var viewModel: PhotoGridViewModel!
    var expectation: XCTestExpectation?
    var isSuccess: Bool = false
    override func setUp() {
        viewModel = PhotoGridViewModel.init(delegate: self)
    }

    override func tearDown() {
        viewModel = nil
        isSuccess = false
    }

    func testFreshImageSearch() {
        expectation = self.expectation(description: "Fetch results for search text")
        
        let searchText = "Apple"
        viewModel.getImagesForSearchText(searchText)
        waitForExpectations(timeout: 5, handler: nil)
        if isSuccess {
            XCTAssertEqual(viewModel.curentPageIndex, 1)
            XCTAssertEqual(viewModel.searchText, searchText)
            XCTAssertFalse(viewModel.imageItems.isEmpty)
            XCTAssertNotNil(viewModel.responseModel)
        } else {
            XCTAssertEqual(viewModel.curentPageIndex, 1)
            XCTAssertTrue(viewModel.imageItems.isEmpty)
            XCTAssertNil(viewModel.responseModel)
        }
    }
    
    func testFetchImagesFromNextPage() {
        expectation = self.expectation(description: "Fetch results for search text")
        
        let searchText = "Apple"

        viewModel.getImagesForSearchText(searchText)
        waitForExpectations(timeout: 5, handler: nil)
        if isSuccess {
            XCTAssertEqual(viewModel.curentPageIndex, 1)
            XCTAssertEqual(viewModel.searchText, searchText)
            XCTAssertFalse(viewModel.imageItems.isEmpty)
            XCTAssertNotNil(viewModel.responseModel)
            
            isSuccess = false // Reset the flag
            expectation = self.expectation(description: "Fetch results for search text")

            let isStarted = viewModel.searchImagesOnNextPage()
            XCTAssertTrue(isStarted)
            waitForExpectations(timeout: 5, handler: nil)
            if isSuccess {
                XCTAssertEqual(viewModel.curentPageIndex, 2, "Page index must increase")
                XCTAssertEqual(viewModel.searchText, searchText, "Search text should not change")
                XCTAssertFalse(viewModel.imageItems.isEmpty)
                XCTAssertNotNil(viewModel.responseModel)
            } else {
                XCTAssertEqual(viewModel.curentPageIndex, 1, "Page index should not increase if requestfailed")
                XCTAssertFalse(viewModel.imageItems.isEmpty, "List must have data from previous page")
            }
        }
    }
    
    func testLockOnMultipleSearchRequest() {
        expectation = self.expectation(description: "Fetch results for search text")
        
        let searchText = "Apple"
        viewModel.getImagesForSearchText(searchText)
        XCTAssertTrue(viewModel.isNextPageRequestInProgress, "lock must be set when request is in progress")
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertFalse(viewModel.isNextPageRequestInProgress, "lock must be reset when request is complete")
    }

    func testInvalidStringSearch() {
        expectation = self.expectation(description: "Fetch results for search text")
        
        let searchText = "ABCD@EFGH#IJ%KLMNOPQR"
        viewModel.getImagesForSearchText(searchText)
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertFalse(isSuccess, "Request must fail as no data is available")
        XCTAssertEqual(viewModel.curentPageIndex, 1)
        XCTAssertTrue(viewModel.imageItems.isEmpty)
        XCTAssertNil(viewModel.responseModel)
    }

    func testEmptyStringSearch() {
        let searchText = ""
        viewModel.getImagesForSearchText(searchText)
        XCTAssertFalse(viewModel.isNextPageRequestInProgress, "We should hit request when search text is empty")
        XCTAssertTrue(viewModel.imageItems.isEmpty)
        XCTAssertNil(viewModel.responseModel)
        XCTAssertEqual(viewModel.searchText, searchText, "We must reset search text")
        
        let isNextRequestStarted = viewModel.searchImagesOnNextPage()
        XCTAssertFalse(isNextRequestStarted, "We should hit request when search text is empty")

    }
}

extension PhotoGridViewModelTests: PhotoGridViewModelDelegate {
    func didUpdateImageItemList() {
        isSuccess = true
        expectation?.fulfill()
    }
    
    func didFailTofetchImageItemList(withError error: Error?, forPage page: Int) {
        isSuccess = false
        expectation?.fulfill()
    }
}
