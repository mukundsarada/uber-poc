//
//  Utilities.swift
//  Uber PhotoTests
//
//  Created by Mukund Sarada on 20/05/19.
//  Copyright © 2019 Uber. All rights reserved.
//

import Foundation

class Utility: NSObject {
    func loadStubFromBundle<T: Decodable>(withName name: String,
                                          extension: String,
                                          responseType: T.Type) -> T? {
        let bundle = Bundle(for: classForCoder)
        let url = bundle.url(forResource: name, withExtension: `extension`)
        if let data = try? Data(contentsOf: url!) {
            return try? JSONDecoder().decode(T.self, from: data)
        }
        return nil
    }
}
