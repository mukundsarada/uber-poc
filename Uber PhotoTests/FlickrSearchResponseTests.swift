//
//  Uber_PhotoTests.swift
//  Uber PhotoTests
//
//  Created by Mukund Sarada on 19/05/19.
//  Copyright © 2019 Uber. All rights reserved.
//

import XCTest
@testable import Uber_Photo

class FlickrSearchResponseTests: XCTestCase {
    var fileReadUtil: Utility!
    
    override func setUp() {
        fileReadUtil = Utility()
    }
    
    override func tearDown() {
        fileReadUtil = nil
    }
    
    func testResponseModelWithValidResponse() {
        let response = fileReadUtil.loadStubFromBundle(withName: "AppleSearchResult",
                                                       extension: "json",
                                                       responseType: FlickrSearchResponse.self)
        XCTAssertNotNil(response)
        XCTAssertTrue(response!.total == "254592")
        XCTAssertTrue(response!.page == 1)
        XCTAssertTrue(response!.pages == 2546)
        XCTAssertTrue(response!.perpage == 100)
        XCTAssertTrue(response!.images.count == 96)
        
        let firstImageItem = response!.images[0]
        XCTAssertNotNil(firstImageItem.imageURL)
        
        let expectedURLString = String(format: NetworkConstants.flickrImageURL,
                                       firstImageItem.farm,
                                       firstImageItem.server,
                                       firstImageItem.id,
                                       firstImageItem.secret)
        XCTAssertEqual(response!.images[0].imageURL!.absoluteString, expectedURLString)
        
        XCTAssertNotNil(response!.images[0].title)
        XCTAssertEqual(response!.images[0].title, "Daily Vocal Resonance Exercises For Singers")
        XCTAssertNotNil(response!.images[0].imageTitle)
    }
    
    func testResponseParseTime() {
        self.measure {
            let _ = fileReadUtil.loadStubFromBundle(withName: "AppleSearchResult",
                                                           extension: "json",
                                                           responseType: FlickrSearchResponse.self)
        }
    }
    
}
