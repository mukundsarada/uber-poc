//
//  ImageCacheManagerTests.swift
//  Uber PhotoTests
//
//  Created by Mukund Sarada on 20/05/19.
//  Copyright © 2019 Uber. All rights reserved.
//

import XCTest
@testable import Uber_Photo

class ImageCacheManagerTests: XCTestCase {
    var cacheManager: ImageCacheManager!
    
    override func setUp() {
        cacheManager = ImageCacheManager.shared
    }

    override func tearDown() {
        cacheManager.clearCache()
    }

    func testImageDownloadFromNetwork() {
        let expectation = self.expectation(description: "Image download")
        var responseImage: UIImage?
        let url = URL(string: "https://farm66.static.flickr.com/65535/47839006132_40c294d292.jpg")!
        cacheManager.loadImage(url,
                               placeholder: nil) { (image, url) in
                                responseImage = image
                                expectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(responseImage)
    }
    
    func testImageFromCache() {
        let expectation1 = self.expectation(description: "Image download")
        var responseImage: UIImage?
        let url = URL(string: "https://farm66.static.flickr.com/65535/47101763134_41f6833095.jpg")!
        cacheManager.loadImage(url,
                               placeholder: nil) { (image, url) in
                                responseImage = image
                                expectation1.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(responseImage)
        
        responseImage = nil
        let expectation2 = self.expectation(description: "Image to be picked from cache")
        cacheManager.loadImage(url,
                               placeholder: nil) { (image, url) in
                                responseImage = image
                                expectation2.fulfill()
        }
        waitForExpectations(timeout: 0.1, handler: nil)
    }
    
    func testImageDownloadWithInvalidURL() {
        let expectation = self.expectation(description: "Image download")
        var responseImage: UIImage?
        let url = URL(string: "https://farm-1.static.flickr.com/65535/47839006132_40c294d292.jpg")!
        cacheManager.loadImage(url,
                               placeholder: nil) { (image, url) in
                                responseImage = image
                                expectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNil(responseImage)
    }
}
